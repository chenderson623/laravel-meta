<?php

ini_set('xdebug.var_display_max_data', 10000);
ini_set('xdebug.var_display_max_depth', 5);

require __DIR__.'/../bootstrap/autoload.php';
$app = require __DIR__.'/../bootstrap/app.php';
$app->make(\Illuminate\Contracts\Console\Kernel::class)->bootstrap();
