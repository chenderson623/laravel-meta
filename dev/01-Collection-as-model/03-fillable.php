<?php

require "../bootstrap.php";

require "MetaEntryModel.php";

$collection = new LaravelMeta\MetaCollection();
$collection->unguard();
$collection->fill([
    'key1' => 'value1',
    'key2' => 'value2',
    'key3' => 'value3',
]);