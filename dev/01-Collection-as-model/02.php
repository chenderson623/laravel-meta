<?php

require "../bootstrap.php";

require "MetaEntryModel.php";

$models = [];
$models[] = new LaravelMeta\MetaEntryModel([
    'key'   => 'key1',
    'value' => 'value1'
]);

$models[] = new LaravelMeta\MetaEntryModel([
    'key'   => 'key2',
    'value' => 'value2'
]);

$models[] = new LaravelMeta\MetaEntryModel([
    'key'   => 'key3',
    'value' => new DateTime()
]);

$models[] = new LaravelMeta\MetaEntryModel([
    'key'   => 'key4',
    'value' => ['subkey1'=>'subvalue1', 'subkey2' => 'subvalue2']
]);

$collection = new LaravelMeta\MetaCollection($models);

var_dump($collection->count());
var_dump($collection['key2']);
var_dump($collection->key2);

$collection->key5 = 'something';
var_dump($collection->key5);

$meta_record = $collection->getMetaRecord('key3');
var_dump($meta_record);
$collection->key3 = 'new value';
var_dump($collection->key3);
var_dump($meta_record);
