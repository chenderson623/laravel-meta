<?php

require "../bootstrap.php";

require "MetaEntryModel.php";

$model1 = new LaravelMeta\MetaEntryModel([
    'key'   => 'key1',
    'value' => 'value1'
]);

$model2 = new LaravelMeta\MetaEntryModel([
    'key'   => 'key2',
    'value' => 'value2'
]);

$model3 = new LaravelMeta\MetaEntryModel([
    'key'   => 'key3',
    'value' => new DateTime()
]);

$model4 = new LaravelMeta\MetaEntryModel([
    'key'   => 'key4',
    'value' => ['subkey1'=>'subvalue1', 'subkey2' => 'subvalue2']
]);


var_dump($model3->toArray());
var_dump($model3['value']);
