

MetaEntryModel:
    override newCollection to use MetaCollection

    Still need a way to query the meta table; possibly even records with the same key
      - need 2 models
          MetaRecord
          MetaTrait - design to pull meta belonging to a record

MetaCollection
    change array access
    change object access
    change indexing to use `key`
    MetaCollection needs a copy of MetaEntryModel so it has the correct table

get:
    get(key, default) - returns value
    getRaw(key) - returns MetaRecord
    __get - calls get
    offestGet - calls  get