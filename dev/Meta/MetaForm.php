<?php

namespace WaveWebsites\models\Meta;

use \Form;
use \Input;

class MetaForm {

 	public static function getMetaFormHtml($meta_data, $meta_definitions, $fields_key, $errors) {
 		$html = '';
 		foreach($meta_definitions as $meta_name=>$meta_definition) {
 			//var_dump($meta_definition);
 			$html.= self::getFieldHtml($meta_name, $meta_definition, $fields_key, $meta_data, $errors);
 		}
 		return $html;
 	}

 	public static function getFieldHtml($name, $meta_definition, $fields_key, $meta_data, $errors) {
 		$type = (isset($meta_definition['type'])) ? $meta_definition['type'] : 'text';
 		switch($type) {
 			case 'text':
 				return self::getTextFieldHtml($name, $meta_definition, $fields_key, $meta_data, $errors);
 				break;
            case 'select':
                return self::getSelectFieldHtml($name, $meta_definition, $fields_key, $meta_data, $errors);
                break;
            case 'upload':
                return self::getUploadFieldHtml($name, $meta_definition, $fields_key, $meta_data, $errors);
                break;
            case 'checkbox':
                return self::getCheckboxFieldHtml($name, $meta_definition, $fields_key, $meta_data, $errors);
                break;
 			case 'textarea':
 				return self::getTextAreaFieldHtml($name, $meta_definition, $fields_key, $meta_data, $errors);
 				break;
 			case 'wysiwyg':
 				return self::getWYSIWYGFieldHtml($name, $meta_definition, $fields_key, $meta_data, $errors);
 				break;
 			default:
 				throw new \Exception("Meta field type [$type] is not defined.");
 		}
 	}

 	public static function getTextFieldHtml($name, $meta_definition, $fields_key, $meta_data, $errors) {
        $options = array_merge([], (isset($meta_definition['options'])) ? $meta_definition['options'] : []);
        $options['name'] = "{$fields_key}[{$name}]";
        $label = (isset($meta_definition['label'])) ? $meta_definition['label'] : $name;
        return Form::bootstrapText(
            $label,
            Input::old("$fields_key.$name", (isset($meta_data[$name])) ? $meta_data[$name] : ''),
            $errors,
            $options
        );
 	}

    public static function getSelectFieldHtml($name, $meta_definition, $fields_key, $meta_data, $errors) {
        $options = array_merge([], (isset($meta_definition['options'])) ? $meta_definition['options'] : []);
        $options['name'] = "{$fields_key}[{$name}]";
        $label = (isset($meta_definition['label'])) ? $meta_definition['label'] : $name;

        // translate list to associate array
        $values = [];
        foreach($meta_definition['values'] as $value) {
            $values[$value] = $value;
        }

        return Form::bootstrapSingleSelect(
            $label,
            $values,
            Input::old("$fields_key.$name", (isset($meta_data[$name])) ? $meta_data[$name] : ''),
            $errors,
            $options
        );
    }

    public static function getUploadFieldHtml($name, $meta_definition, $fields_key, $meta_data, $errors) {
        $options = array_merge([], (isset($meta_definition['options'])) ? $meta_definition['options'] : []);
        $options['name'] = "{$fields_key}[{$name}_new]";
        $label = (isset($meta_definition['label'])) ? $meta_definition['label'] : $name;
        return Form::bootstrapUpload(
            $label,
            Input::old("$fields_key.$name", (isset($meta_data[$name])) ? $meta_data[$name] : ''),
            $errors,
            $options
        );
    }

    public static function getCheckboxFieldHtml($name, $meta_definition, $fields_key, $meta_data, $errors) {
        $options = array_merge([], (isset($meta_definition['options'])) ? $meta_definition['options'] : []);
        $options['name'] = "{$fields_key}[{$name}]";
        $label = (isset($meta_definition['label'])) ? $meta_definition['label'] : $name;
        $value = (isset($meta_definition['options']['value'])) ?$meta_definition['options']['value'] : 1;
        return Form::bootstrapCheckbox(
            $label,
            $value,
            Input::old("$fields_key.$name", (isset($meta_data[$name])) ? $meta_data[$name] : ''),
            $errors,
            $options
        );
    }

 	public static function getTextAreaFieldHtml($name, $meta_definition, $fields_key, $meta_data, $errors) {
        $options = array_merge(['rows'=>5], (isset($meta_definitions['options'])) ? $meta_definitions['options'] : []);
        $options['name'] = "{$fields_key}[{$name}]";
        $label = (isset($meta_definition['label'])) ? $meta_definition['label'] : $name;
        return Form::bootstrapTextArea(
            $label,
            Input::old("$fields_key.$name", (isset($meta_data[$name])) ? $meta_data[$name] : ''),
            $errors,
            $options
        );
 	}

 	public static function getWYSIWYGFieldHtml($name, $meta_definition, $fields_key, $meta_data, $errors) {
        $options = array_merge(['height'=>200], (isset($meta_definitions['options'])) ? $meta_definitions['options'] : []);
        $options['name'] = "{$fields_key}[{$name}]";
        $label = (isset($meta_definition['label'])) ? $meta_definition['label'] : $name;
        return Form::bootstrapWYSIWYGEditor(
            $label,
            Input::old("$fields_key.$name", (isset($meta_data[$name])) ? $meta_data[$name] : ''),
            $errors,
            $options
        );
 	}

}