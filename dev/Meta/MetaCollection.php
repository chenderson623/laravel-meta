<?php

namespace WaveWebsites\models\Meta;

use Illuminate\Support\Collection as BaseCollection;

/**
 * Need to override offsetGet so that we can call a non-existent key through array access
 */
class MetaCollection extends BaseCollection
{

    public function offsetGet($key)
    {
        return $this->get($key);
    }


}