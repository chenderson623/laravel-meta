<?php

namespace App\Models;

use App\EblastServer\EmailAddresses\EmailAddress;

class SubscriberProfile extends \Eloquent {

    protected $fillable = ['email', 'domain', 'first_name', 'last_name', 'telephone', 'active', 'date_added', 'date_removed', 'hash', 'remote_checked', 'remote_valid'];

    protected static $hash_salt = '08a648be28c84adcbbb006859528049d';

    protected $subscriber_subscriptions_set;


}
