<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserMetaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('user_meta', function(Blueprint $table)
        {
            $table->increments('id');

            $table->integer('user_id')->unsigned()->index();

            $table->string('type')->default('null');

            $table->string('key')->index();
            $table->text('value');

            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_meta');
    }
}
